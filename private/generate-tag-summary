#!/usr/bin/perl

# Copyright © 2017, 2019 Chris Lamb <lamby@debian.org>

use v5.20;
use warnings;
use utf8;
use autodie;

use Cwd qw(realpath);
use File::Basename qw(dirname);

# neither Path::This nor lib::relative are in Debian
use constant THISFILE => realpath __FILE__;
use constant THISDIR => dirname realpath __FILE__;

# use Lintian modules that belong to this program
use lib THISDIR . '/../lib';

use Const::Fast;
use Getopt::Long;
use IPC::Run3;

use Lintian::IPC::Run3 qw(safe_qx);

const my $PLUS => q{+};
const my $WAIT_STATUS_SHIFT => 8;

my (%added, %removed, %opt);

my %opthash = ('in-place|i' => \$opt{'in-place'},);

# init commandline parser
Getopt::Long::config('bundling', 'no_getopt_compat', 'no_auto_abbrev',
    'permute');

# process commandline options
Getopt::Long::GetOptions(%opthash)
  or die("error parsing options\n");

my ($commit_range) = @ARGV;
if (not $commit_range) {
    my $describe = safe_qx(qw(git describe --abbrev=0));
    my $status = $? >> $WAIT_STATUS_SHIFT;

    die "git describe failed with code $status\n"
      if $status;

    chomp($describe);
    if (not $describe) {
        die "git describe did not return anything.\n";
    }
    $commit_range = "${describe}..HEAD";
    print "Assuming commit range to be: ${commit_range}\n";
}

my $output;
my @command =(qw{git diff}, $commit_range, qw{-- tags/*/*.tag});
run3(\@command, \undef, \$output);

my @lines = split(/\n/, $output);
while (defined(my $line = shift @lines)) {

    next
      unless $line =~ m{ \A ([\+-]) Tag: \s*+ ([^ ]++) \s*+ \Z}xsm;

    my ($change, $tag) = ($1, $2);
    if ($change eq $PLUS) {
        $added{$tag} = 1;
    } else {
        $removed{$tag} = 1;
    }
}

for my $tag (keys(%added)) {
    if (exists($removed{$tag})) {
        # Added and removed?  More likely, the tag was moved between
        # two files.
        delete($added{$tag});
        delete($removed{$tag});
    }
}

if (not %added and not %removed) {
    print {*STDERR} "No tags were added or removed\n";
}

if ($opt{'in-place'}) {
    my $matched = 0;
    open(my $in_fd, '<', 'debian/changelog');
    open(my $out_fd, '>', 'debian/changelog.tmp');
    while (my $line = <$in_fd>) {
        chomp $line;
        if ($line =~ m/^  \* WIP\b/) {
            emit_tag_summary($out_fd);
            $matched++;
        } else {
            print {$out_fd} $line . "\n";
        }
    }
    close($out_fd);
    close($in_fd);
    if ($matched != 1) {
        die("changelog did not match WIP placeholder exactly once\n");
    }
    rename('debian/changelog.tmp', 'debian/changelog');
    print "Updated debian/changelog\n";
} else {
    emit_tag_summary(\*STDOUT);
}

sub emit_tag_summary {
    my ($fd) = @_;

    if (%added or %removed) {
        print {$fd} "  * Summary of tag changes:\n";
    }
    if (%added) {
        print {$fd} "    + Added:\n";
        for my $tag (sort(keys(%added))) {
            print {$fd} "      - $tag\n";
        }
    }
    if (%removed) {
        print {$fd} "    + Removed:\n";
        for my $tag (sort(keys(%removed))) {
            print {$fd} "      - $tag\n";
        }
    }
    return;
}

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
